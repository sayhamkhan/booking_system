<nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="#">
              <span data-feather="home"></span>
              Dashboard <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('booking')}}">
              <span data-feather="file"></span>
              Bookings
            </a>
          </li>
          <li class="nav-item">
        <a class="nav-link" href="{{route('buses')}}">
              <span data-feather="shopping-cart"></span>
             Coach
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="{{route('customer')}}">
              <span data-feather="users"></span>
              Customers
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="{{route('showall')}}">
              <span data-feather="layers"></span>
             Locations
              <ul> 
                  <li >
                    <a href="{{route('routeslocation')}}">
                      Route Details
                    </a>
                 </li>
              </ul>
            </a>    
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('user')}}">
              <span data-feather="users"></span>
              User
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('report')}}">
              <span data-feather="layers"></span>
              Reports
            </a>
          </li>
        </ul>
      </div>
    </nav>