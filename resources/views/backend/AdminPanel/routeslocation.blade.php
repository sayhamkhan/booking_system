@extends('backend.master')

@section('content')
 <style>
  h1{
    text-align: center;
  };
  
</style>

<!-- Button trigger modal -->


<h1>Routes Details</h1> 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Routes
</button> <br><br>  
 <div class="table-responsive" >
    <table class="table table-striped" id="orderTable">
      <thead>
        <tr>
          <th>Serial</th>
          <th>From</th>
          <th>To</th>
          <th>Arrival</th>
          <th>Depature</th>                                                
       </tr>
      </thead>
      <tbody>
             @foreach($routeslocation as $key=>$routeslocation)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$routeslocation->from}}</td>
            <td>{{$routeslocation->to}}</td>
            <td>{{$routeslocation->arrival}}</td>
            <td>{{$routeslocation->depature}}</td>
          </tr>
            @endforeach
      </tbody>
    </table>
</div>

<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Routes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('routescreate')}}" method="POST">
      @csrf
        <div class="modal-body">
      	  <div class="form-group">
      	    <label for="formGroupExampleInput">From</label>
            <select class="browser-default form-control" name="from" id="origin">    
                   @foreach($locations as $location)
                    <option value="{{$location->name}}" >
                        {{$location->name}}
                    </option>
                    @endforeach
            </select>
            <label for="formGroupExampleInput">To</label>
            <select class="browser-default form-control" name="to" id="origin">    
                   @foreach($locations as $location)
                    <option value="{{$location->name}}" >
                        {{$location->name}}
                    </option>
                    @endforeach
            </select>
            <label for="formGroupExampleInput">Arrival</label>
            <input type="text" class="form-control" name="arrival" id="formGroupExampleInput" placeholder="Arrival">
            <label for="formGroupExampleInput">Depature</label>
            <input type="text" class="form-control" name="depature" id="formGroupExampleInput" placeholder="Depature">
            
      	  </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Routes</button>
        </div>
      </form>
    </div>
  </div>
</div>


        


@endsection