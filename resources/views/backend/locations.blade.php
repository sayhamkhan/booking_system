@extends('backend.master')

@section('content')

  <style>
  h1{
    text-align: center;
  };
  
</style>

<!-- Button trigger modal -->


<h1>Locations Details</h1> 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Locations
</button> <br><br>      

        
 <div class="table-responsive" >
    <table class="table table-striped" id="orderTable">
      <thead>
        <tr>
          <th>Serial</th>
          <th>Name</th>
          <th>Status</th>   
          <th>Address</th>   
          <th>Mobile</th>   
          <th>Action</th>                                               
       </tr>
      </thead>
      <tbody>
            @foreach($location as $key=>$data)
            <tr>
            <td>{{$key+1}}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->status}}</td>
            <td>{{$data->address}}</td>
            <td>{{$data->mobile}}</td>
            <td>   
              <a href="{{route('editlocation', $data->id)}}" class="btn btn-success">Edit</a>
              <a href="{{route('deletelocation',$data->id)}}" class="btn btn-danger">Delete</a>
            </td>
            </tr>
             @endforeach
      </tbody>
    </table>
</div>


<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('location')}}" method="POST">
      @csrf
           <div class="modal-body">
        <div class="modal-body">
          <div class="form-group">
            <label for="formGroupExampleInput">Location Name</label>
            <input type="text" class="form-control" name="name" id="formGroupExampleInput" placeholder="Location Name">
          </div>
          <!-- <div class="form-group">
            <label for="formGroupExampleInput">Status</label>
            <input type="text" class="form-control" name="status" id="formGroupExampleInput" placeholder="Status">
          </div> -->
          <div class="form-group">
            <label for="formGroupExampleInput">Address</label>
            <input type="text" class="form-control" name="address" id="formGroupExampleInput" placeholder="Address">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput">Mobile</label>
            <input type="text" class="form-control" name="mobile" id="formGroupExampleInput" placeholder="Mobile">
          </div>
          <div> 
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@stop

@section('script')
  <script>
    $(document).ready(function(){
      $('#orderTable').DataTable();
    });

  </script>
@endsection