    <footer class="footer-wrapper bg-dark ">

        <!-- footer top area start -->
        <div class="footer-top">
            <div class="container">
                <div class="footer-features-inner">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item footer-feature-item">
                                <div class="features-icon">
                                    <i class="ion-ios-telephone"></i>
                                </div>
                                <div class="features-content">
                                    <h5>01777862036</h5>
                                    <p>Free support line!</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item footer-feature-item">
                                <div class="features-icon">
                                    <i class="ion-help-buoy"></i>
                                </div>
                                <div class="features-content">
                                    <h5>mail@kodeeo.com</h5>
                                    <p>Orders Support!</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item footer-feature-item">
                                <div class="features-icon">
                                    <i class="ion-clock"></i>
                                </div>
                                <div class="features-content">
                                    <h5>Mon - Fri / 8:00 - 18:00</h5>
                                    <p>Working Days/Hours!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer top area start -->

        <!-- footer bottom area start -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="copyright-text text-center">
                            <p>Copyright © 2019 <a href="#">Kodeeo</a>. All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer bottom area end -->

    </footer>