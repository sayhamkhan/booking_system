

<!-- Start Header Area -->
<header class="header-area">

  
  <!-- main header start -->
  <div class="main-header d-none d-lg-block">
    <!-- header top start -->
    <div class="header-top theme-color-4">
      <div class="container bdr-bottom-4">
        <div class="row align-items-center">
        </div>
        <div class="col-lg-6">
          <div class="header-links">
            <ul class="nav justify-content-end">
              <li>Welcome to Kodeeo Paribahan</li>
              
              @guest()
              <li>
                <a href="" data-toggle="modal" data-target="#registrationModal">Create Account</a>
              </li>
               <li>
                <a href="" data-toggle="modal" data-target="#loginModal">User Login</a>
              </li>
              @endguest
              @auth()
                <li><a href="{{route('dologout')}}">LOGOUT</a></li>
              @endauth
              </div>

                

            </li>

          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- header top end -->

<!-- header middle area start -->
<div class="header-middle-area theme-color-4">
  <div class="container">
    <div class="row align-items-center">
      <!-- start logo area -->
      <div class="col-lg-3">
        <div class="logo">
          <h3 class="text-light">Kodeeo Paribahan</h3>
        </div>
      </div>
      <!-- start logo area -->

      <!-- start search box area -->
      <div class="col-lg-5">
        <div class="search-box-wrapper">
          <form class="search-box-inner">

          </form>
        </div>
      </div>
      <!-- start search box end -->

      <!-- mini cart area start -->
      <div class="col-lg-4">
        <div class="header-configure-wrapper">
          <div class="support-inner">
            <div class="support-icon">
              <i class="ion-android-call"></i>
            </div>
            <div class="support-info">
              <p>Free support call:</p>
              <strong>+88 01777862036</strong>
            </div>
          </div>

        </div>
      </div>
      <!-- mini cart area end -->
    </div>
  </div>
</div>
<!-- header middle area end -->


<!-- main menu area start -->
<div class="main-menu-area bg-white sticky">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-3"></div>
      <div class="col-lg-9">
        <div class="main-menu home-main">
          <!-- main menu navbar start -->
          <nav class="desktop-menu">
            <ul>
              <li class="{{ (request()->is('/')) ? 'active' : '' }}"><a href="{{route('home')}}">Home</a></li>
              <li><a href="{{route('flocation')}}">Locations </a></li>
              <!-- <li><a href="#">Contact us </a></li> -->
              <li  class="{{ (request()->is('/ubooking')) ? 'active' : '' }}"><a href="{{route('ubooking')}}">Booking System </a></li>
              <li><a href="{{route('printTicket')}}">Print Ticket</a></li>

            </ul>
          </nav>
          <!-- main menu navbar end -->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- main menu area end -->
</div>
<!-- main header start -->


</header>
<!-- end Header Area -->
<!-- login modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">User Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('show_login')}}" method="POST" >
            @csrf
            <div class="form-group">
              <label for="email">E-mail</label>
              <input class="form-control" required type="text" name="email" placeholder="email">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input class="form-control" type="password" required name="password" placeholder="password">
            </div>

            <div class="modal-footer">
              <button class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-warning" type="submit">Login</button>

            </div>

          </form>
      </div>
      
    </div>
  </div>
</div>

<!-- registration modal -->
<div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">User Registration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('createregistration')}}" method="post">
          @csrf
          <div class="form-group">
            <label for="name">Name</label>
            <input class="form-control" type="text" name="name" placeholder="Your Name">
          </div>
          <div class="form-group">
            <label for="email">E-mail</label>
            <input class="form-control" type="text" name="email" placeholder="email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input class="form-control" type="text" name="password" placeholder="Password">
          </div>
          <div class="form-group">            
            <label for="address">Address:</label>            
            <input class="form-control" type="text" name="address" placeholder="Enter address" />
          </div>
          <div class="form-group">            
            <label class="contact">Contact:</label>            
            <input class="form-control" type="text" name="contact" placeholder="Enter contact" required/>       
          </div>
          <div class="form-group">            
            <label>Gender:</label>            
            <select class="form-control" name="gender" required />                
            <option value="male">Male</option>                
            <option value="female">Female</option>            
          </select>       
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button class="btn btn-warning" type="submit">Registration</button>
        </div>
      </form>
    </div>

  </div>
</div>
</div>
