<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>Kodeeo Paribahan</title>

    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="{{ url ('assets/img/favicon.ico')}}" type="image/x-icon" />

    <!-- Google fonts include -->
    <link href="{{ url('https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,900')}}" rel="stylesheet">
    <link href="{{url('/css/vendor.css')}}" rel="stylesheet">
   

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" media="screen"> -->
    <link href="{{asset('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">    

    <!-- Main Style CSS -->
    <link href="{{ url('/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('/assets/fonts/ionicons28b5.eot')}}" rel="stylesheet">
    <link href="{{ asset('/assets/fonts/ionicons28b5.svg')}}" rel="stylesheet">
    <link href="{{ asset('/assets/fonts/ionicons28b5.ttf')}}" rel="stylesheet">
    <link href="{{ asset('/assets/fonts/ionicons28b5.woff')}}" rel="stylesheet">
    
    <!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>


<body>
@include('frontend.partials.header')

@if ($message = Session::get('message'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
</div>
@endif
            

  



    <!-- main wrapper start -->
    <main class="body-bg">

        <!-- feature product area end -->
        @yield('content')

    </main>
    <!-- main wrapper end -->

    <!--== Start Footer Area Wrapper ==-->
@include('frontend.partials.footer')

    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>


    <script type="text/javascript" src="{{ asset('/js/jquery-3.4.1.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/moment.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
   

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>



    <script src="{{ asset('/js/vendor.js') }}"></script>
    <script src="{{ asset('/js/active.js') }}"></script>
   

    @yield('after_script')
</body>

</html>