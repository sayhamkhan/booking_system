@extends ('frontend.master')

<style>
  .trip-list {
    color: #333;
  }
  .single-trip {
    display: flex;
    border: 1px solid #c5c5c5;
    align-items: center;
  }

  .single-trip {
    margin-bottom: 15px;
  }
  .trip-section {
    padding: 10px;
    flex: 1;
  }
</style>
@section('content')
<br>

<section>
<div class="container">
@foreach($buses as $key=>$bus)


<div class="trip-list">
  <div class="single-trip">
    <div class="trip-section">
      Coach Id:  {{$bus->coach_id}}
    </div>
    <div class="trip-section">From:: {{$bus->routelocation->from}}
    </div>
    <div class="trip-section">To:: {{$bus->routelocation->to}}
    </div>
    <div class="trip-section">Reporting:: {{$bus->reporting}}
    </div>
    <div class="trip-section">Depature:: {{$bus->depature}}
    </div>
    <div class="trip-section">Boarding:: {{$bus->boarding}}
    </div>
    <div class="trip-section">Price:: {{$bus->price}}
    </div>
    <div class="trip-section">
      <a href="{{route('showseat',$bus->id)}}" class="btn btn-primary">Available Seats</a>
    </div>
  </div>
</div>


@endforeach
</div>
</section><br>
@stop