<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Frontend routes

Route::get('/', 'Frontend\HomeController@home')->name('home');
Route::get('/login', 'Frontend\LoginController@Login');
Route::post('/login/hello','Frontend\LoginController@dologin')->name('show_login');
Route::get('/logout', 'Frontend\LoginController@dologout')->name('dologout');

Route::get('/ubooking','Frontend\UBookingController@ubooking')->name('ubooking');
Route::post('/search/coach','Frontend\UBookingController@ubookingPost')->name('post.ubooking');
Route::post('/ubookingCancle','Frontend\UBookingController@ubookingCancle')->name('ubookingCancle');

//Route::get('/register', 'Frontend\RegistrationController@register')->name('registration');
Route::post('/createregister', 'Frontend\RegistrationController@createregistration')->name('createregistration');
Route::get('/showseat/{id}', 'Frontend\SeatController@showseat')->name('showseat');
Route::get('/cancleSeat/{id}', 'Frontend\SeatController@cancleSeat')->name('cancleSeat');
Route::post('/ticket', 'Frontend\SeatController@ticket')->name('ticket');
Route::post('/showinfo', 'Frontend\SeatController@finalinfo')->name('showinfo');
Route::get('/flocation','Frontend\FlocationController@flocation')->name('flocation');
//Print Ticket
Route::get('/printTicket','Frontend\UBookingController@printTicket')->name('printTicket');
Route::get('/downloadPDF/{id}','Frontend\UBookingController@downloadPDF')->name('downloadPDF');

// Route::get('/show', 'Frontend\SearchController@show')->name('show');

//backend
Route::get('/admin', 'Backend\AdminController@admin')->name('dashboard');
Route::get('/booking','Backend\BookingController@booking')->name('booking');
Route::get('/buses','Backend\BusesController@buses')->name('buses');
Route::post('/addBuses','Backend\BusesController@addBuses')->name('addBuses');
Route::get('/customer','Backend\CustomersController@customers')->name('customer');
Route::get('/routes','Backend\RoutesController@routes')->name('routes');
Route::get('/location','Backend\RoutesController@showall')->name('showall');
Route::get('/editlocation/{id}','Backend\RoutesController@editlocation')->name('editlocation');
// Route::post('/editlocationprocess/{id}','Backend\RoutesController@editlocation')->name('editlocation');
Route::post('/editlocationprocess/{id}','Backend\RoutesController@editlocationprocess')->name('editlocationprocess');
Route::get('deletelocation/{id}','Backend\RoutesController@deletelocation')->name('deletelocation');
Route::post('/routes','Backend\RoutesController@location')->name('location');
Route::post('/createRoutes','Backend\RoutesController@createRoutes')->name('createRoutes');
Route::get('/report','Backend\ReportController@report')->name('report');
Route::get('/routeslocation','Backend\RoutesdetailsController@routeslocation')->name('routeslocation');
Route::post('/routescreate','Backend\RoutesdetailsController@routescreate')->name('routescreate');
Route::get('/locationdetails','Backend\LocationdetailsController@locationdetails')->name('locationdetails');
Route::get('/user','Backend\UserController@user')->name('user');

//Route::post('/adminlogout', 'Frontend\LoginController@adminlogout')->name('adminlogout');
