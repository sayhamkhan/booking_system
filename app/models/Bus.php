<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    protected $fillable=['coach_id','route_id','reporting','depature','boarding','coach_type','price','time'];

    public function routelocation(){

    	return $this->hasOne(Routelocation::class, 'id','route_id');
    }


}
