<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Location;

class HomeController extends Controller
{
    public function home()
   {
   		$locations=Location::all();
   		// dd($locations);
		return view('frontend.layouts.home',compact('locations'));

   }
}
