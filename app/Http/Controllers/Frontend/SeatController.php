<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Routelocation;
use App\models\Bus;
use App\models\Booking;
use App\models\Reservation;
use Illuminate\Support\Facades\Auth;
use Session;
use Carbon\Carbon;


class SeatController extends Controller
{
    public function showseat($id)
    {    	
    	$bookingDate=Session::get('bookingdate');    	
    	$coach_id=Session::get('coach');
    	$reserved_seat= array();
    	$allSeat=Reservation::where('date', $bookingDate)->where('coach_id', $coach_id)->get();
    	foreach ($allSeat as $key => $value) {
    		
    		$reserved_seat=array_merge($reserved_seat,json_decode($value->seat));
    		//$reserved_seat=json_decode($value->seat);
    	}
    	//dd($reserved_seat);
    	
    	

    	return view ('frontend.layouts.showseat',compact('id','reserved_seat'));
    }
    public function cancleSeat($id)
    {
        $bookingdate=Session::get('bookingdate');
        $user_id=auth()->user()->id;
        $coach_id=$id;
        $booking=Booking::where('date', $bookingdate)->where('user_id', $user_id)->where('coach_id', $coach_id)->first();
        $reservation_id=Reservation::where('id', ($booking->id))->first();
        Booking::find($booking->id)->delete();
        Reservation::find($reservation_id->id)->delete(); 
        //dd($reservation_id);
        
        return redirect()->route('home');
    }


    public function finalinfo(Request $request)
    {
    	//dd($request->all());
    	$totalSeat=Count($request->input('seat'));
    	$seatNumber=($request->input('seat'));
        $buses=Bus::find($request->input('bus_id'));
        $coach_id=$buses->coach_id;
        //dd($coach_id);
    	$totalPrice=(($buses->price)*$totalSeat);
    	$user_id= Auth::id();

//temporary data
    	Session::put('seats',$seatNumber);
    	Session::put('coach',$coach_id);
		Session::put('order.quantity',$totalSeat);
		Session::put('order.unit_price',$buses->price);
		Session::put('order.total_amount',$totalPrice);
		    	//dd($user_id);


    	return view ('frontend.layouts.info',compact('buses', 'totalPrice', 'seatNumber'));
    }

    public function ticket(Request $request)
    {
    	$coach_id=Session::get('coach');
    	$user_id=auth()->user()->id;
    	$date=Carbon::now()->format('Y-m-d');
    	$data=Session::get('order');
    	$seat=Session::get('seats');
    	$seatNumber=json_encode($seat);
    	$bookingDate=Session::get('bookingdate');
    	//dd($seatNumber);
    

    	$booking_id=Booking::create([
			'date' => $bookingDate,
			'coach_id' => $coach_id,
			'unit_price' => $data['unit_price'],
			'quantity' => $data['quantity'],
			'total_amount' => $data['total_amount'],
			'transaction_id' => $request->input('bkash'),
    	]);
//manage the seats
		$reservation=Reservation::create([
            'user_id' => $user_id,
			'coach_id' => $coach_id,
			'booking_id' => $booking_id->id,
			'seat' => $seatNumber,
			'date' => $bookingDate,
		]);

    	return redirect()->route('home');
    }
}
