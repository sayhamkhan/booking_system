<?php

namespace App\Http\Controllers\Frontend;

use App\models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Booking;
use App\models\Location;
use App\models\Bus;
use App\models\Routelocation;
use Session;
use PDF;

class UBookingController extends Controller
{
   public function ubooking()
   {
       $locations= Location::all();
       return view('frontend.layouts.ubooking',compact('locations'));
   }

   
   public function ubookingPost(Request $request)
   {
        //dd($request->all());
    $date=$request->date;
    $bookingDate=Session::put('bookingdate',$date);
        //dd($bookingDate);
    
  		//$route_id=get route id from route table using Route model, where location from and location to came from user form.

    $buses='';
    //routeslocations matches with from&to....
    $route=Routelocation::where('from',$request->input('pick_point'))
    ->where('to',$request->input('drop_point'))
    ->first();

//how many bus available or not...
    if(!empty($route)){
        $route_id=$route->id;

        $buses=Bus::with('routelocation')->where('route_id',$route_id)
        ->where('coach_type',$request->input('coach_type'))
        ->where('time',$request->input('time'))->get();
        
    }

    
    return view('frontend.layouts.show',compact('buses'));

}
public function ubookingCancle(Request $request)
{
        //dd($request->all());
    $date=$request->date;
    $bookingDate=Session::put('bookingdate',$date);
        //dd($bookingDate);
    
        //$route_id=get route id from route table using Route model, where location from and location to came from user form.
    $buses='';
    $route=Routelocation::where('from',$request->input('pick_point'))
    ->where('to',$request->input('drop_point'))
    ->first();

    if(!empty($route)){
        $route_id=$route->id;

        $buses=Bus::with('routelocation')->where('route_id',$route_id)
        ->where('coach_type',$request->input('coach_type'))->get();
        
    }

    
    return view('frontend.layouts.cancleShow',compact('buses'));
}
public function printTicket()
{
    $userId=auth()->user()->id;
    $data=Reservation::where('user_id', $userId)->with('booking')->with('user')->with('coach')->latest()->get();
    //dd($data);
    return view('frontend.print.printTicket', compact('data'));
}
public function downloadPDF($id)
{
    //dd($id);
    $downloadPdf = Reservation::find($id)->with('booking')->with('user')->with('coach')->first();
    //dd($downloadPdf);

    $pdf = PDF::loadView('frontend.print.pdfTicket', compact('downloadPdf'));
    return $pdf->download('invoice.pdf');
}

}
