<?php

namespace App\Http\Controllers\Backend;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Routelocation;
use App\models\Location;

class RoutesdetailsController extends Controller
{
        public function routeslocation()
        {
        $locations= Location::all();
   		$routeslocation=Routelocation::all();
    	//$routeslocation= 98;
    	//dd($routeslocation);
    	return view ('Backend.AdminPanel.routeslocation', compact('locations','routeslocation'));
       }
  
	public function routescreate(Request $request)
    {

    	$data=[];
    	$data = [
    		'from' => $request->input('from'),        
            'to' => $request->input('to'), 
            'arrival' => $request->input('arrival'),    
            'depature' => $request->input('depature'),
    	];

    	Routelocation::create($data);
    	return redirect()->back();
    	//return view('backend.buses');
    }


}
