<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationdetailsController extends Controller
{
    public function locationdetails()
    {
    	return view('Backend.AdminPanel.locationdetails');
    }
}
