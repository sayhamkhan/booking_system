<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Bus;
use App\models\Location;
use App\models\Routelocation;

class BusesController extends Controller
{
    public function buses()
    {

        $routes = Routelocation::all();
    	
        $buses= Bus::all();
    	//dd($buses);
    	return view ('Backend.buses', compact('buses','routes'));
    }
  
	public function addBuses(Request $request)
    {

    	// dd($request->all());


    	$data = [
    		'coach_id' => $request->input('coach_id'),        
            'reporting' => $request->input('reporting'),
            'route_id' => $request->input('route_id'),   
            'depature' => $request->input('depature'),
            'boarding' => $request->input('boarding'),
            'coach_type' => $request->input('coach_type'),  
            'time'=>$request->input('time'),  
            'price' => $request->input('price'),

    	];
        //dd($data);

    	Bus::create($data);
    	return redirect()->back();
    	//return view('backend.buses');
    }
}
