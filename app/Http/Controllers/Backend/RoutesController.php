<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Route;
use App\models\Location;
class RoutesController extends Controller
{
    public function routes()
    {
        $routes = route::all();

    	return view('Backend.locations',compact('routes'));
    }
	public function createRoutes(Request $request)
    {

    	$data=[];
    	$data = [
            'route_location' => $request->input('route_location'),
            'arrival_time' => $request->input('arrival_time'),
            'depature_time' => $request->input('depature_time'),
            'stop_no' => $request->input('stop_no'),
    	];

    	Route::create($data);
        return redirect()->back();
    	//return view('backend.routes');
    }


    public function location(Request $request)
    {

        $data = [


                'name' => $request->input('name'),
                'status' => "Active",
                'address' => $request->input('address'),
                'mobile' => $request->input('mobile'),


        ];
              

           $routes = Location::create($data);
            return redirect()->back();



    }

    public function showall()

    {

        $location = Location::all();

        return view('backend.locations',compact('location'));

    }
    
    public function editlocation($id)
    {
        $locations=Location::where('id', $id)->first();
        return view('backend.editlocation', compact('locations'));
    }
    

    public function editlocationprocess(Request $request, $id)
    {
          $data = [


                'name' => $request->input('name'),
                'status' => $request->input('status'),


        ];

        $routes = Location::where('id', $id)->update($data);
        return redirect()->route('showall');


    }

    public function deletelocation($id)

    {
        Location::find($id)->delete();
        return redirect()->back();
    }


}
