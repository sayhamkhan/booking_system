<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\user;

class UserController extends Controller
{
    public function user()
    {
    	$user= User::all();
    	return view ('Backend.partials.user',compact('user'));
    }
}
