<?php $__env->startSection('content'); ?>



<?php $__env->startSection('content'); ?>

  <style>
  h1{
    text-align: center;
  };
  
</style>

<!-- Button trigger modal -->

<h1>Booking Details</h1> 
        
 <div class="table-responsive" >
    <table class="table table-striped" id="orderTable">
      <thead>
        <tr>
          <th>Serial</th>
          <th>User Name</th>
          <th>Coach Number</th>
          <th>Unit Price</th>
          <th>Seat</th>
          <th>Quantity</th>
          <th>Total Amount</th>
          <th>Transaction Id</th>                                                
       </tr>
      </thead>
      <tbody>
             <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
            <td><?php echo e($key+1); ?></td>
            <td><?php echo e($bus->user->name); ?></td>
            <td><?php echo e($bus->coach_id); ?></td>
            <td><?php echo e($bus->booking->unit_price); ?></td>
            <td><?php echo e($bus->seat); ?></td>
            <td><?php echo e($bus->booking->quantity); ?></td>
            <td><?php echo e($bus->booking->total_amount); ?></td>
            <td><?php echo e($bus->booking->transaction_id); ?></td>
          </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </tbody>
    </table>
</div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
  $(document).ready(function(){
    $('#orderTable').DataTable();
  });

</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('backend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('backend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\booking-system\resources\views/backend/booking.blade.php ENDPATH**/ ?>