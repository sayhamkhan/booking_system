<?php $__env->startSection('content'); ?>


<div class="container mt-3">
  <br>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">Buy Ticket</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">Cancel Ticket</a>
  </li>
</ul>
<div class="tab-content">
    <div id="home" class="container tab-pane active">
        <form action="<?php echo e(route('post.ubooking')); ?>" id="sky-form" class="" method="POST">
            <div class="all_con d-flex">
                <!-- <form action="" method="POST" role="form"> -->
                <?php echo csrf_field(); ?>
                <div class="single_cont">                
                    <select class="browser-default" name="pick_point" id="origin">
                        <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($location->name); ?>" >
                            <?php echo e($location->name); ?>

                        </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="single_cont">                
                    <select class="browser-default" name="drop_point" id="origin">
                        <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($location->name); ?>" >
                           <?php echo e($location->name); ?>

                       </option>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="single_cont" style="position: relative;">
                    
                       <input id="datepicker1" class="form-control" type="date" name="date">
                       
                   
                </div>
                <div class="single_cont">                
                    <select class="browser-default custom-select" name="time" id="origin">
                        <option value="" >Time Period</option>
                        <option value="Morning" >Morning(06.00 AM-11.59 AM)</option>
                        <option value="Evening" >Evening(12.00 PM-03.59 PM)</option>
                        <option value="Night" >Night(04.00 PM-11.00 PM)</option>
                    </select>
                </div>
                <div class="single_cont">                
                    <select class=" form-control browser-default custom-select" name="coach_type" id="origin">
                       
                        <option selected value="AC" >
                            AC
                        </option>
                        <option value="NON-AC" >
                            NON-AC
                        </option>
                    </select>
                </div>
                <div class="single_cont">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <div id="menu1" class="container tab-pane">
        <form action="<?php echo e(route('ubookingCancle')); ?>" id="sky-form" class="" method="POST">
            <?php echo csrf_field(); ?>
            <div class="all_con d-flex">
                <!-- <form action="" method="POST" role="form"> -->
                <?php echo csrf_field(); ?>
                <div class="single_cont">                
                    <select class="browser-default" name="pick_point" id="origin">
                        <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($location->name); ?>" >
                            <?php echo e($location->name); ?>

                        </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="single_cont">                
                    <select class="browser-default" name="drop_point" id="origin">
                        <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($location->name); ?>" >
                           <?php echo e($location->name); ?>

                       </option>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="single_cont">
                    <div>
                        <input id="datepicker2" class="form-control" type="date" name="date">
                        
                    </div>
                </div>
                <div class="single_cont">                
                    <select class="browser-default custom-select" name="time" id="origin" required>
                        
                        <option value="morning" >Morning(06.00 AM-11.59 AM)</option>
                        <option value="evening" >Evening(12.00 PM-03.59 PM)</option>
                        <option value="night" >Night(04.00 PM-11.00 PM)</option>
                    </select>
                </div>
                <div class="single_cont">                
                    <select class=" form-control browser-default custom-select" name="coach_type" id="origin">
                       
                        <option selected value="AC" >
                            AC
                        </option>
                        <option selected value="NON-AC" >
                            NON-AC
                        </option>
                    </select>
                </div>
                <div class="single_cont">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- service features end -->








<!-- banner statistics start -->
<div class="banner-statistics-area pt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="img-container">
                    <a href="#"><img src="<?php echo e(url('assets/img/banner/img1_home4.jpg')); ?>" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_script'); ?>

         

<script type="text/javascript">

      var input = document.getElementById("datepicker1");
      var today = new Date();
      var day = today.getDate();
      // Set month to string to add leading 0
      var mon = new String(today.getMonth()+1); //January is 0!
      var yr = today.getFullYear();
      
        if(mon.length < 2) { mon = "0" + mon; }
      
        var date = new String( yr + '-' + mon + '-' + day );
      
      input.disabled = false; 
      input.setAttribute('min', date);
        

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\booking-system\resources\views/frontend/layouts/ubooking.blade.php ENDPATH**/ ?>