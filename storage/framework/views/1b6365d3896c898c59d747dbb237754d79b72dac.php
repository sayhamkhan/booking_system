<?php $__env->startSection('content'); ?>

<!-- <?php if($message=Session::get('message')): ?>
<div class="row justify-content-center">
    <p class="alert alert-success alert-block"><?php echo e($message); ?></p>
</div>
<?php endif; ?>


   -->      <!-- slider area start -->
        <section class="slider-area">
            <div class="hero-slider-active slick-arrow-style slick-dot-style">
                <!-- single slider item start -->
                <div class="hero-slider-item">
                    <div class="d-flex align-items-center bg-img h-100" data-bg="<?php echo e(url('assets/img/slider/home1-slide1.jpg')); ?>">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-2 col-md-10 offset-md-2">
                                    <div class="hero-slider-content">
                                        <h2>BEST & MOST PROFESSIONAL TRAVEL OPTION IN BANGLADESH</h2>
                                        <h3>We have the widest range of luxury inter-city bus.</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single slider item end -->

                <!-- single slider item start -->
                <div class="hero-slider-item">
                    <div class="d-flex align-items-center bg-img h-100" data-bg="<?php echo e(url('assets/img/slider/1.jpg')); ?>">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-2 col-md-10 offset-md-2">
                                    <div class="hero-slider-content">
                                        <h2>BEST & MOST PROFESSIONAL TRAVEL OPTION IN BANGLADESH</h2>
                                        <h3>We have the widest range of luxury inter-city bus.</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single slider item start -->
            </div>
        </section>
        <!-- slider area end -->

        <!-- service features start -->
        <section class="service-features pt-50">
            <div class="container">
                <div class="service-features-inner bg-white">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item">
                                <div class="features-icon">
                                    <i class="ion-android-sync"></i>
                                </div>
                                <div class="features-content">
                                    <h5>Online booking</h5>
                                    <p>You can book your seat at home!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col lg-4 col-md-4">
                            <div class="single-features-item">
                                <div class="features-icon">
                                    <i class="ion-social-usd"></i>
                                </div>
                                <div class="features-content">
                                    <h5>Best Service</h5>
                                    <p>We ensure travellers safety first!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item">
                                <div class="features-icon">
                                    <i class="ion-help-buoy"></i>
                                </div>
                                <div class="features-content">
                                    <h5>support 24/7</h5>
                                    <p>We support 24 hours a day!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- banner statistics end -->

        <!-- features categories area start -->
        <section class="features-categories-area pt-50">
            <div class="container">
                <div class="section-wrapper bg-white">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-header">
                                <!-- section title start -->
                                <div class="section-title">
                                    <h4>Choose your destination (AC)</h4>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 mt-5">
                            <div class="feature-product-slider">
                                <div class="features-product-carousel slick-arrow-style">
                                    <!-- product item start -->
                                    <div class="product-slide-item">
                                        <div class="product-item">
                                            <div class="product-thumb">
                                                <a href="product-details.html">
                                                    <img src="<?php echo e(url('assets/img/places/4.jpg')); ?>" alt="">
                                                </a>
                                                
                                            </div>
                                            <!-- <div class="product-content">
                                                <h5 class="product-name"><a href="#">Khulna</a></h5>
                                                <div class="price-box">
                                                    <span class="price-regular">৳1050.00</span>
                                                </div>
                                                <div class="product-item-action">
                                                    <a class="btn btn-cart" href="#"><i class="ion-bag"></i> Book Now</a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                    <!-- product item start -->

                                    <!-- product item start -->
                                    <div class="product-slide-item">
                                        <div class="product-item">
                                            <div class="product-thumb">
                                                <a href="product-details.html">
                                                <img src="<?php echo e(url('assets/img/places/5.jpg')); ?>" alt="">
                                                </a>
                                                
                                            </div>
                                            <!-- <div class="product-content">
                                                <h5 class="product-name"><a href="#">joust duffle bag</a></h5>
                                                <div class="price-box">
                                                    <span class="price-regular">৳1050.00</span>
                                                </div>
                                                <div class="product-item-action">
                                                    <a class="btn btn-cart" href="#"><i class="ion-bag"></i> Book Now</a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                    <!-- product item start -->

                                    <!-- product item start -->
                                    <div class="product-slide-item">
                                        <div class="product-item">
                                            <div class="product-thumb">
                                                <a href="product-details.html">
                                                    <img src="<?php echo e(url('assets/img/places/6.jpg')); ?>" alt="">
                                                </a>
                                                
                                            </div>
                                            <!-- <div class="product-content">
                                                <h4 class="product-name"><a href="#">Khulna</a></h4>
                                                <p class="product-desc">Lorem ipsum dolor sit amet..</p>
                                                <div class="price-box">
                                                    <span class="price-regular">৳1050.00</span>
                                                </div>
                                                <a class="btn btn-cart" href="#"><i class="ion-bag"></i> Book Now</a>
                                                </div>
                                        </div> -->
                                    </div>
                                </div>
                                    <!-- product item start -->

                                   
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
        <!-- features categories area end -->

        

      

        <!-- feature product area start -->
        <div class="feature-product-area pt-50 pb-50">
            <div class="container">
                <div class="feature-product-wrapper bg-white">
                    <div class="row align-items-center">
                        
                        
                    </div>
                </div>
            </div>
        </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Localhost\bookingsystem\resources\views/frontend/layouts/home.blade.php ENDPATH**/ ?>