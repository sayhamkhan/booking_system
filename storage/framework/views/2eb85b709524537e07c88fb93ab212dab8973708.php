<?php $__env->startSection('content'); ?>
 <style>
  h1{
    text-align: center;
  };
  
</style>

<!-- Button trigger modal -->


<h1>Routes Details</h1> 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Routes
</button> <br><br>  
 <div class="table-responsive" >
    <table class="table table-striped" id="orderTable">
      <thead>
        <tr>
          <th>Serial</th>
          <th>From</th>
          <th>To</th>
          <th>Arrival</th>
          <th>Depature</th>                                                
       </tr>
      </thead>
      <tbody>
             <?php $__currentLoopData = $routeslocation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$routeslocation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
            <td><?php echo e($key+1); ?></td>
            <td><?php echo e($routeslocation->from); ?></td>
            <td><?php echo e($routeslocation->to); ?></td>
            <td><?php echo e($routeslocation->arrival); ?></td>
            <td><?php echo e($routeslocation->depature); ?></td>
          </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </tbody>
    </table>
</div>

<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Routes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo e(route('routescreate')); ?>" method="POST">
      <?php echo csrf_field(); ?>
        <div class="modal-body">
      	  <div class="form-group">
      	    <label for="formGroupExampleInput">From</label>
            <select class="browser-default form-control" name="from" id="origin">    
                   <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($location->name); ?>" >
                        <?php echo e($location->name); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            <label for="formGroupExampleInput">To</label>
            <select class="browser-default form-control" name="to" id="origin">    
                   <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($location->name); ?>" >
                        <?php echo e($location->name); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            <label for="formGroupExampleInput">Arrival</label>
            <input type="text" class="form-control" name="arrival" id="formGroupExampleInput" placeholder="Arrival">
            <label for="formGroupExampleInput">Depature</label>
            <input type="text" class="form-control" name="depature" id="formGroupExampleInput" placeholder="Depature">
            
      	  </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Routes</button>
        </div>
      </form>
    </div>
  </div>
</div>


        


<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\booking-system\resources\views/Backend/AdminPanel/routeslocation.blade.php ENDPATH**/ ?>