<?php $__env->startSection('content'); ?>

  <style>
  h1{
    text-align: center;
  };
  
</style>

<!-- Button trigger modal -->

<h1>Coach Details</h1> 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Add Coach
</button> <br><br>  

        
 <div class="table-responsive" >
    <table class="table table-striped" id="orderTable">
      <thead>
        <tr>
          <th>Serial</th>
          <th>Coach Id</th>
          <th>Route Name</th>
          <th>Reporting</th>
          <th>Depature</th>
          <th>Boarding</th>
          <th>Coach Type</th>
          <th>Time</th>
          <th>Price</th>                                                  
       </tr>
      </thead>
      <tbody>
             <?php $__currentLoopData = $buses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
            <td><?php echo e($key+1); ?></td>
            <td><?php echo e($bus->coach_id); ?></td>
            <td><?php echo e($bus->routelocation->from); ?> - <?php echo e($bus->routelocation->to); ?></td>
            <td><?php echo e($bus->reporting); ?></td>
            <td><?php echo e($bus->depature); ?></td>
            <td><?php echo e($bus->boarding); ?></td>
            <td><?php echo e($bus->coach_type); ?></td>
            <td><?php echo e($bus->time); ?></td>
            <td><?php echo e($bus->price); ?></td>
          </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </tbody>
    </table>
</div>

<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Coach</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo e(route('addBuses')); ?>" method="post">
      <?php echo csrf_field(); ?>
        <div class="modal-body">
      	  <div class="form-group">
            <label for="formGroupExampleInput">Coach Id</label>
            <input type="text" class="form-control" name="coach_id" id="formGroupExampleInput" placeholder="Coach Id">
            <label for="formGroupExampleInput">Route Id</label>

           <select class="browser-default form-control" name="route_id" id="origin">    
                   <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($route->id); ?>">
                        <?php echo e($route->from); ?> to <?php echo e($route->to); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            <label for="formGroupExampleInput">Reporting</label>
            <input type="text" class="form-control" name="reporting" id="formGroupExampleInput" placeholder="Reporting">
            <label for="formGroupExampleInput">Depature</label>
            <input type="text" class="form-control" name="depature" id="formGroupExampleInput" placeholder="Depature">
            <label for="formGroupExampleInput">Boarding</label>
            <input type="text" class="form-control" name="boarding" id="formGroupExampleInput" placeholder="Boarding">
            <label for="formGroupExampleInput">Couch Type</label>
            <!-- <input type="text" class="form-control" name="coach_type" id="formGroupExampleInput" placeholder="Couch Type"> -->
            <select class="browser-default form-control" id="origin" name="coach_type" id="">
              <option value="AC" >AC</option>
              <option value="Non-AC" >Non-AC</option>
            </select>

            <label for="formGroupExampleInput">Time</label>

           <select class="browser-default form-control" name="time" id="origin">
            <option value="morning" >Morning(06.00 AM-11.59 AM)</option>
            <option value="evening" >Evening(12.00 PM-03.59 PM)</option>
            <option value="night" >Night(04.00 PM-11.00 PM)</option>
            </select>

            <label for="formGroupExampleInput">Price</label>
            <input type="text" class="form-control" name="price" id="formGroupExampleInput" placeholder="Price">
      	  </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Coach</button>
        </div>
      </form>
    </div>
  </div>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
  $(document).ready(function(){
    $('#orderTable').DataTable();
  });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\booking-system\resources\views/Backend/buses.blade.php ENDPATH**/ ?>