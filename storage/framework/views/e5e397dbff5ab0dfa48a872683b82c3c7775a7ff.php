<?php $__env->startSection('content'); ?>
    <section>
        <div class="container">
            <h1>Booking Details</h1>
            <br><br>

            <div class="table-responsive" >
                <table class="table table-striped" id="orderTable">
                    <thead>
                    <tr>
                        <th>Serial</th>
                        <th>From</th>
                        <th>From</th>
                        <th>Coach Number</th>
                        <th>Unit Price</th>
                        <th>Seat</th>
                        <th>Quantity</th>
                        <th>Total Amount</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($key+1); ?></td>
                            <td><?php echo e($bus->coach->routelocation->from); ?></td>
                            <td><?php echo e($bus->coach->routelocation->to); ?></td>
                            <td><?php echo e($bus->coach_id); ?></td>
                            <td><?php echo e($bus->booking->unit_price); ?></td>
                            <td><?php echo e($bus->seat); ?></td>
                            <td><?php echo e($bus->booking->quantity); ?></td>
                            <td><?php echo e($bus->booking->total_amount); ?></td>
                            <td>
                                <a href="<?php echo e(route('downloadPDF', $bus->id)); ?>">Print</a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\localhost\bookingsystem\resources\views/frontend/print/printTicket.blade.php ENDPATH**/ ?>